# myEllipsis #
![Alt text](http://image6.buzzintown.com/files/venue/upload_8000/upload_original/373653-ellipsis.jpg)

* `It repeatedly tries to remove the last word of the text until it reaches the desired size.`
*  function name is `ellip();`
```
This function ``ellip();`` is taking as parameter the CSS class myEllipsis.
This means you need to add myEllipsis css class to the element you want to ellipse
```




### Usage:












[Wiki] (https://bitbucket.org/fporusniuc/myellipsis/wiki/Home)